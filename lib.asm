%macro JMP_IF_SPACE 2
    cmp %1, ' '
    je  %2
    cmp %1, 0x9
    je  %2
%endmacro

%macro SAVE_VAL 1-*
%rep %0
    push    %1
%rotate 1
%endrep
%endmacro

%macro GET_VAL 1-*
%rep %0
%rotate -1
    pop %1
%endrep
%endmacro



section .text

global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

;-----------------------------------
; Принимает код возврата и завершает текущий процесс
;-----------------------------------
exit:
    mov rax, 60
    syscall

;-----------------------------------
; Принимает указатель на нуль-терминированную строку, возвращает её длину
;-----------------------------------

string_length:
    mov rax, rdi
    str_len_loop:
        mov dl, [rax]
        inc rax
        test    dl, dl
        jnz str_len_loop

        dec rax
        sub rax, rdi
        ret

;-----------------------------------
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
;-----------------------------------

print_string:
    mov rsi, 1
print_universal:
    push rsi
    push rdi
    call string_length
    pop rsi ; Address
    pop rdi ; std___
    mov rdx, rax ; Length
    mov rax, 1 ; write
    syscall
    ret
print_error:
    mov rsi, 2
    jmp print_universal
;-----------------------------------
; Переводит строку (выводит символ с кодом 0xA)
;-----------------------------------

print_newline:
    mov rdi, '\n'

;-----------------------------------
; Принимает код символа и выводит его в stdout
;-----------------------------------

print_char:
    push rdi
    mov rsi, rsp
    mov rdi, 1 ; STDOUT
    mov rax, 1 ; SYS_WRITE
    mov rdx, 1
    syscall
    pop rdi
    ret


;-----------------------------------
; Выводит знаковое 8-байтовое число в десятичном формате 
;-----------------------------------
print_int:
    test rdi, rdi
    jnl .pi_uint
    neg rdi
    SAVE_VAL rdi
    mov rdi, '-'
    call print_char
    GET_VAL rdi
    .pi_uint:  
    call print_uint
    ret




;-----------------------------------
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
;-----------------------------------

print_uint:
    sub rsp, 32
    mov rax, rdi
    mov r8, 10
    lea rcx, [rsp+20]
    mov byte [rcx], 0

    check_number:
        test    rax, rax
        jnz print_uint_loop
        mov rax, 0x30
        dec rcx
        mov byte [rcx], al
        jmp end_print

    print_uint_loop:
        test    rax, rax
        jz  end_print
        xor rdx, rdx
        div r8
        add rdx, 0x30
        dec rcx
        mov [rcx], dl
        jmp print_uint_loop

    end_print:
        lea rdi, [rcx]
        call    print_string
        add rsp, 32
        ret

;-----------------------------------
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
;-----------------------------------
string_equals:
    xor r8, r8

    .loop:
        movzx rax, byte [rdi +r8]
        cmp al, byte [rsi + r8]  
        jne .n_eq

        test rax, rax             
        jz .eq

        inc r8
            jmp .loop
    
    .eq:
        mov rax, 1
        ret
    .n_eq:
        xor rax, rax
        ret


;-----------------------------------
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
;-----------------------------------
read_char:
    mov rax, 0  ; SYS_READ
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    xor rdx, rdx
    inc rdx
    syscall
    test    rax, rax
    jz  read_char_end
    xor rax, rax
    mov al, [rsp]
    read_char_end:
        inc rsp
        ret


;-----------------------------------
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
;-----------------------------------
read_word:
    SAVE_VAL   rbx, r12, r13
    mov r12, rdi
    xor r13, r13
    mov rbx, rsi
    word_start:
        call    read_char
        JMP_IF_SPACE    al, word_start

    word_loop:
        test    al, al
        jz  word_success
        cmp rbx, r13
        jbe word_fail
        mov byte [r12+r13], al
        call    read_char
        inc r13
        JMP_IF_SPACE    al, word_success
        jmp word_loop

    word_success:
        mov byte [r12+r13], 0
        mov rax, r12
        mov rdx, r13
        GET_VAL   rbx, r12, r13
        ret

    word_fail:
        xor rax, rax
        GET_VAL   rbx, r12, r13
        ret

;-----------------------------------
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
;-----------------------------------
parse_uint:
    xor rax, rax
    xor r8, r8
    mov r9, 10
    parse_loop:
        mov cl, [rdi+r8]
        cmp cl, '0'
        jb  parse_end
        cmp cl, '9'
        ja  parse_end
        sub cl, '0'
        mul r9
        add rax, rcx
        inc r8
        jmp parse_loop
    parse_end:
        mov rdx, r8
        ret
;-----------------------------------
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
;-----------------------------------
parse_int:
    SAVE_VAL   rbx, r12, r13
    mov rbx, rdi
    mov r12b, [rdi]
    mov r13, rdi
    cmp r12b, '0'
    jb  check_sign
    cmp r12b, '9'
    jbe start_parse
    jmp parse_int_fail

    check_sign:
        cmp r12b, '-'
        je  sign_found
        cmp r12b, '+'
        jne parse_int_fail
    sign_found:
        inc r13

    start_parse:
        mov rdi, r13
        call    parse_uint
        test    rdx, rdx
        jz  parse_int_fail
        cmp r13, rbx
        je  parse_int_end
        cmp r12b, '-'
        jne positive_number
        neg rax
        inc rdx
        jmp parse_int_end

    positive_number:
        cmp r12b, '+'
        jne parse_int_fail
        inc rdx

    parse_int_fail:
        xor rdx, rdx

    parse_int_end:
        GET_VAL   rbx, r12, r13
        ret 

;-----------------------------------
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
;-----------------------------------
string_copy:
    SAVE_VAL   rbx, r12, r13
    mov rbx, rdi
    mov r12, rsi
    mov r13, rdx

    call    string_length

    cmp rax, r13
    jb  .ok

    xor rax, rax
    GET_VAL   rbx, r12, r13
    ret

    .ok:
    mov rcx, rax
    inc rcx
    mov rsi, rbx
    mov rdi, r12
    rep movsb

    GET_VAL   rbx, r12, r13
    ret
