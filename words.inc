%include "colon.inc"

section .data

colon "fifth", fifth
db "value 5", 0

colon "fourth", fourth
db "value 4", 0

colon "third", third
db "value 3", 0

colon "second", second
db "value 2", 0

colon "first", first
db "value 1", 0
