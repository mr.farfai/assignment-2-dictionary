result: main.o lib.o dict.o
	ld -o $@ $^

%.o: %.asm
	nasm -f elf64 -o $@ $<

main.o: main.asm dict.inc colon.inc lib.inc words.inc

dict.o: dict.asm lib.inc

tests: result
	python3 test.py

clean:
	rm -rf *.o

.PHONY: tests clean
